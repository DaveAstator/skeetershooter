﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public bool isDragging;
    public Vector3 startpos;
    public Vector2 startpos2d;
    public float xRate = 1f;
    public float yRate = 1f;
    void MouseControl()
    {

        if (Input.GetMouseButtonDown(0))
        {
            isDragging = true;
            startpos = Input.mousePosition;

        }
        if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
        }
        if (Input.GetMouseButton(0))
        {
            if (isDragging)
            {
                var curpos = startpos - Input.mousePosition;
                gameObject.transform.eulerAngles += new Vector3(curpos.y * yRate, -curpos.x * xRate, 0);
                startpos = Input.mousePosition;
            }
        }
    }

    void MouseControlIncremental()
    {

        if (Input.GetMouseButtonDown(0))
        {
            isDragging = true;
            startpos = Input.mousePosition;

        }
        if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
        }
        if (Input.GetMouseButton(0))
        {
            if (isDragging)
            {
                var curpos = startpos - Input.mousePosition;
                gameObject.transform.eulerAngles += new Vector3(curpos.y * yRate * 0.1f, -curpos.x * xRate * 0.1f, 0);
            }
        }
    }
    private void Update()
    {

#if UNITY_EDITOR
        MouseControl();
#else
        TouchControl();
#endif

    }
    private void TouchControl()
    {
        //Check count touches
        if (Input.touchCount > 0)
        {
            //Touch began, save position
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                startpos2d = Input.GetTouch(0).position;
            }
            //Move finger by screen
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var curpos = startpos2d - Input.GetTouch(0).position;
                gameObject.transform.eulerAngles += new Vector3(curpos.y * yRate, -curpos.x * xRate, 0);
                startpos2d = Input.GetTouch(0).position;
            }
        }
    }
    private void TouchControlIncremental()
    {
        //Check count touches
        if (Input.touchCount > 0)
        {
            //Touch began, save position
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                startpos2d = Input.GetTouch(0).position;
            }
            //Move finger by screen
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var curpos = startpos2d - Input.GetTouch(0).position;
                gameObject.transform.eulerAngles += new Vector3(curpos.y * yRate * 0.05f, -curpos.x * xRate * 0.05f, 0);
            }
        }
    }



}
