﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyShotMechanic : MonoBehaviour
{
    public GameObject aimingLogicObj;
    public GameObject plateFactory;
    private IShooter shooter;

    // Start is called before the first frame update
    private void Awake()
    {
        shooter = aimingLogicObj.GetComponent<IShooter>();
        plateFactory.GetComponent<IPlateFactory>().OnPlateCreated += RegisterNewPlate;
    }

    private void RegisterNewPlate(GameObject obj)
    {
        obj.GetComponent<IShootable>().OnPreFall += DoLuckyShot;

    }

    private void DoLuckyShot(GameObject obj)
    {

        shooter.LuckyShootTarget(obj);
        obj.GetComponent<IShootable>().OnPreFall -= DoLuckyShot;
    }

}
