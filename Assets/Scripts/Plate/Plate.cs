﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Plate : MonoBehaviour, IShootable
{
    public float despawnTime = 1f;
    public float flightTime = 0f;
    private float timeInSights = 0f;
    public float preFallTime = 0.1f;

    public event Action<GameObject> OnPreFall=delegate { };
    public event Action<GameObject> OnSuccess = delegate { };
    public event Action<GameObject> OnFail = delegate { };

 
    // Start is called before the first frame update
    void Start()
    {

    }


    private void OnCollisionEnter(Collision collision)
    {
       /* GetComponent<Rigidbody>().velocity = new Vector3(
            GetComponent<Rigidbody>().velocity.x,
            1f,
            GetComponent<Rigidbody>().velocity.z
            );*/
        OnFail(gameObject);
        gameObject.GetComponent<PlateDeath>().DelayedDeath(despawnTime);
        print($"flight time {GetFlightTime()}");

    }
    private void OnTriggerEnter(Collider other)
    {
        print("collide!");
        //   Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (PredictFall())
        {
            OnPreFall(gameObject); //  Destroy(gameObject);
        }
        flightTime += Time.deltaTime;
    }
    public bool PredictFall()
    {
        //disregarding acceleration.
        //var hits = Physics.RaycastAll(transform.position, Vector3.down);
        var hits = Physics.RaycastAll(transform.position, GetComponent<Rigidbody>().velocity);
        foreach (var hit in hits)
        {
            TerrainCollider land = hit.transform.gameObject.GetComponent<TerrainCollider>();
            if (land != null)
            {
                if ((GetComponent<Rigidbody>().velocity.magnitude * preFallTime) >= hit.distance)
                {
                    
                    //print(GetComponent<Rigidbody>().velocity.magnitude * preFallTime);
                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }
    public void GetKilled()
    {
        OnSuccess(gameObject);
        GetComponent<PlateDeath>().InstantDeath(3f);
        //Destroy(gameObject);
    }

    public float GetFlightTime()
    {
        return flightTime; 
    }

    public void RegisterTimeInSights(float time)
    {
        timeInSights += time;
    }

    public float GetTimeInSights()
    {
        return timeInSights;
    }
}
