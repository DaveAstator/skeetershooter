﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IPlateFactory
{
    event Action<GameObject> OnPlateCreated;
}
public class PlateLauncher : MonoBehaviour, IPlateFactory
{
    // Start is called before the first frame update
    public event Action<GameObject> OnPlateCreated;
    public GameObject platePrefab;
    //public GameObject exitPoint;
    public List<GameObject> exitPoints;
    public float startForce=10f;
    public bool fireAtWill = true;
    [Range(0,90)]
    public float horizontalSpread_deg;
    [Range(0, 90)]
    public float verticalSpread_deg;
    public UnityEvent OnPlateDone;
    void Start()
    {
       // LaunchPlate();
    }

    public void LaunchPlate()
    {
        var exitPoint = exitPoints[UnityEngine.Random.Range(0, exitPoints.Count)];
        GameObject newPlate = Instantiate(platePrefab);
        newPlate.transform.position = exitPoint.transform.position;
        newPlate.transform.rotation = exitPoint.transform.rotation;
        newPlate.transform.eulerAngles = new Vector3
            (
            newPlate.transform.eulerAngles.x + UnityEngine.Random.Range(-verticalSpread_deg, verticalSpread_deg),
            newPlate.transform.eulerAngles.y + UnityEngine.Random.Range(-horizontalSpread_deg, horizontalSpread_deg),
            newPlate.transform.eulerAngles.z
            );
        //gob.transform.localScale = exitPoint.transform.localScale;
        newPlate.GetComponent<Rigidbody>().velocity = newPlate.transform.forward * startForce;
        OnPlateCreated(newPlate);
        newPlate.GetComponent<IShootable>().OnFail += TerminatePlate;
        newPlate.GetComponent<IShootable>().OnSuccess += TerminatePlate;
    }

    public void TerminatePlate(GameObject plate)
    {
        OnPlateDone.Invoke();
    }
    // Update is called once per frame
    void Update()
    {
        if (!fireAtWill) return;
        if (Time.frameCount % 20 ==0)
        {
            LaunchPlate();
        }
        
    }
}
