﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateDeath : MonoBehaviour
{
    public ParticleSystem DeathEffect;
    private IEnumerator DelayedDestroy(float sec)
    {

        Destroy(GetComponent<Plate>());
        print("started dying");
        yield return new WaitForSeconds(sec);
        Destroy(gameObject);
    }
    // Start is called before the first frame update
    public void InstantDeath(float time_sec)
    {
        DeathEffect.Play();
        GetComponent<MeshRenderer>().enabled = false;
        GetComponentInChildren<LineRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        print("started dying proc");
        StartCoroutine(DelayedDestroy(time_sec));

    }
    public void DelayedDeath(float time_sec)
    {
        print("started dying proc");
        StartCoroutine(DelayedDestroy(time_sec));
    }
}
