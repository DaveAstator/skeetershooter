﻿using UnityEngine;

public interface IShooter
{
    void LuckyShootTarget(GameObject target);
    void ShootTarget(GameObject other);
}
