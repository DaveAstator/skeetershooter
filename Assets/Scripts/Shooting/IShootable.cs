﻿using System;
using UnityEngine;

public interface IShootable
{
    event Action<GameObject> OnPreFall;
    event Action<GameObject> OnSuccess;
    event Action<GameObject> OnFail;

    void GetKilled();
    float GetFlightTime();
    void RegisterTimeInSights(float time);
    float GetTimeInSights();
}
