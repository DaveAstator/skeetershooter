﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootingGameLogic : MonoBehaviour , IShooter

{
    // Start is called before the first frame update
    
    public GameObject indicator;
    private IShootIndicator indicatorScript;
    public soTimeTable lockonTimes;
    //private float targetLockonTime_sec=0.5f;
    public float targetInSightTime_sec = 0f;
    public bool isTargetInSight=false;
    public List<GameObject> targets=new List<GameObject>();
    public int lastFrameWithTargets = 0;

    public UnityEvent OnShot;
    public UnityEvent OnLuckyShot;
    private void Awake()
    {
        indicatorScript = indicator.GetComponent<IShootIndicator>();
    }
    void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.GetComponent<IShootable>() != null)
        {
            targets.Add(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.gameObject.GetComponent<IShootable>() != null)
        {
            targets.Remove(other.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.gameObject.GetComponent<IShootable>() != null)
        {
            float requiredLockonTime_sec = 99f;
            requiredLockonTime_sec = GetRequiredLockonTime(other.gameObject);

            float completePercent = (targetInSightTime_sec / requiredLockonTime_sec);
            // print($"times: {targetLockonTime_sec},{ targetInSightTime_sec}");
            indicatorScript.UpdateByPercent(completePercent);

            if (targetInSightTime_sec >= requiredLockonTime_sec)
            {

                ShootTarget(other.gameObject);
                return;
            }
            other.GetComponent<IShootable>().RegisterTimeInSights(Time.deltaTime);
            targetInSightTime_sec += Time.deltaTime;
            lastFrameWithTargets = Time.frameCount;
        }
    }

    private float GetRequiredLockonTime(GameObject plate)
    {
        float flightTime = plate.GetComponent<IShootable>().GetFlightTime();
        foreach (lockonTimeEntry item in lockonTimes.data)
        {
            
            if (item.secondFrom < flightTime)
                if (item.secondTo > flightTime)
                    return item.lockonTime;
        }
        return 99f;
    }

    public void ShootTarget(GameObject other)
    {
        other.GetComponent<IShootable>()?.GetKilled();
        targetInSightTime_sec = 0;
        OnShot.Invoke();
        return;
    }

    public void LuckyShootTarget(GameObject target)
    {
        float requiredLockonTime_sec = GetRequiredLockonTime(target);         

        if (targets.Contains(target))
        {
            
            var remainingTime = requiredLockonTime_sec-targetInSightTime_sec;
            //float time = target.GetComponent<IShootable>().GetTimeInSights();
            //time = Mathf.Floor(time * 10) / 10f;
            //time = time - Mathf.Floor(time);
            if (Random.Range(0f, 1f) > remainingTime)
            {
                print("Its your luckyy day");
                ShootTarget(target);
            }
            else
            {
                print("Lucky shot failed");
            }
            OnLuckyShot.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //in case objects disappear from game and our sights. wroks for debug also.
        if ((Time.frameCount - lastFrameWithTargets) > 2)
        {
            targetInSightTime_sec = 0;
            targets.Clear();
            indicatorScript.UpdateByPercent(0);
        }
        //indicatorScript.UpdateByPercent(targetInSightTime_sec / 1f);

    }
}
