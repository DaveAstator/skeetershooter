﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTrailFX : MonoBehaviour
{
    Vector3 pos2;
    Vector3 pos1;


    void Start()
    {
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, transform.position - transform.parent.GetComponent<Rigidbody>().velocity * 0.3f);
        pos1 = transform.position;
    }

    void Update()
    {
        if (Time.frameCount % 2 == 0)
        {
            GetComponent<LineRenderer>().SetPosition(0, transform.position);
            GetComponent<LineRenderer>().SetPosition(1, transform.position - transform.parent.GetComponent<Rigidbody>().velocity * 0.3f);
            pos1 = transform.position;
        }
    }
}
