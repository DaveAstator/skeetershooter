﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class AimingPhysics : MonoBehaviour
{
    public GameObject crosshair;
    public GameObject coneObject;
    [Range(0f,0.3f)]
    public float crosshairRadius;
    public float scanDistance = 100f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    { 
        
        //probably dont need to execute every frame
        float coneScale = scanDistance*crosshairRadius;
        crosshair.transform.localScale = new Vector3(crosshairRadius, crosshairRadius, crosshairRadius);
        coneObject.transform.localScale = new Vector3(coneScale, coneScale, scanDistance);
    }
}
