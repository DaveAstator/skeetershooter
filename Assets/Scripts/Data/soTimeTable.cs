﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public struct lockonTimeEntry{
    [SerializeField] public float secondFrom;
    [SerializeField] public float secondTo;
    [SerializeField] public float lockonTime;
}
[CreateAssetMenu(fileName="newTimeTable",menuName ="skeetershooter/so_timetable")]

[Serializable]
public class soTimeTable : ScriptableObject
{
    [SerializeField] public List<lockonTimeEntry> data;
}
