<Q                         INSTANCING_ON      LIGHTMAP_SHADOW_MIXING     POINT_COOKIE   SHADOWS_CUBE   SHADOWS_SOFT      _ALPHATEST_ON   �  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    float4 _LightPositionRange;
    float4 _LightProjectionParams;
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 _LightColor0;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler samplerunity_NHxRoughness [[ sampler (0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (1) ]],
    sampler sampler_LightTexture0 [[ sampler (2) ]],
    sampler sampler_LightTextureB0 [[ sampler (3) ]],
    sampler sampler_TerrainNormalmapTexture [[ sampler (4) ]],
    sampler sampler_TerrainHolesTexture [[ sampler (5) ]],
    sampler sampler_MainTex [[ sampler (6) ]],
    sampler sampler_MetallicTex [[ sampler (7) ]],
    texture2d<half, access::sample > _TerrainHolesTexture [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    texture2d<half, access::sample > _MetallicTex [[ texture(2) ]] ,
    texture2d<half, access::sample > _TerrainNormalmapTexture [[ texture(3) ]] ,
    texture2d<float, access::sample > _LightTextureB0 [[ texture(4) ]] ,
    texturecube<float, access::sample > _LightTexture0 [[ texture(5) ]] ,
    texture2d<float, access::sample > unity_NHxRoughness [[ texture(6) ]] ,
    depthcube<float, access::sample > _ShadowMapTexture [[ texture(7) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half u_xlat16_0;
    bool u_xlatb0;
    float4 u_xlat1;
    half4 u_xlat16_1;
    float3 u_xlat2;
    half3 u_xlat16_3;
    half3 u_xlat16_4;
    float3 u_xlat5;
    half u_xlat16_5;
    float u_xlat15;
    half u_xlat16_18;
    u_xlat0.x = float(_TerrainHolesTexture.sample(sampler_TerrainHolesTexture, input.TEXCOORD4.xy).x);
    u_xlatb0 = u_xlat0.x==0.0;
    if(((int(u_xlatb0) * int(0xffffffffu)))!=0){discard_fragment();}
    u_xlat0.xyz = input.TEXCOORD3.xyz + (-FGlobals._LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-FGlobals._LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * FGlobals._LightProjectionParams.w;
    u_xlat15 = FGlobals._LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-FGlobals._LightProjectionParams.x);
    u_xlat15 = (-u_xlat15) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz + float3(0.0078125, 0.0078125, 0.0078125);
    u_xlat1.x = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xyz, saturate(u_xlat15), level(0.0)));
    u_xlat2.xyz = u_xlat0.xyz + float3(-0.0078125, -0.0078125, 0.0078125);
    u_xlat1.y = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xyz, saturate(u_xlat15), level(0.0)));
    u_xlat2.xyz = u_xlat0.xyz + float3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + float3(0.0078125, -0.0078125, -0.0078125);
    u_xlat1.w = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat0.xyz, saturate(u_xlat15), level(0.0)));
    u_xlat1.z = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xyz, saturate(u_xlat15), level(0.0)));
    u_xlat0.x = dot(u_xlat1, float4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-FGlobals._LightShadowData.x) + half(1.0);
    u_xlat16_3.x = half(fma(u_xlat0.x, float(u_xlat16_3.x), float(FGlobals._LightShadowData.x)));
    u_xlat0.xyz = input.TEXCOORD3.xyz + (-FGlobals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = FGlobals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = FGlobals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = FGlobals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-input.TEXCOORD3.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = fma(FGlobals.unity_ShadowFadeCenterAndType.w, u_xlat0.x, u_xlat1.x);
    u_xlat0.x = fma(u_xlat0.x, float(FGlobals._LightShadowData.z), float(FGlobals._LightShadowData.w));
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat16_3.x = half(u_xlat0.x + float(u_xlat16_3.x));
    u_xlat16_3.x = clamp(u_xlat16_3.x, 0.0h, 1.0h);
    u_xlat1.xyz = input.TEXCOORD3.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, input.TEXCOORD3.xxx, u_xlat1.xyz);
    u_xlat1.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, input.TEXCOORD3.zzz, u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz + FGlobals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = _LightTexture0.sample(sampler_LightTexture0, u_xlat1.xyz).w;
    u_xlat0.x = _LightTextureB0.sample(sampler_LightTextureB0, u_xlat0.xx).x;
    u_xlat0.x = u_xlat1.x * u_xlat0.x;
    u_xlat0.x = float(u_xlat16_3.x) * u_xlat0.x;
    u_xlat16_3.xyz = half3(u_xlat0.xxx * float3(FGlobals._LightColor0.xyz));
    u_xlat16_1.xyz = _TerrainNormalmapTexture.sample(sampler_TerrainNormalmapTexture, input.TEXCOORD4.zw).xyz;
    u_xlat16_1.xyz = fma(u_xlat16_1.xzy, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -1.0));
    u_xlat16_0 = dot(u_xlat16_1.xyz, u_xlat16_1.xyz);
    u_xlat16_0 = rsqrt(u_xlat16_0);
    u_xlat16_1.xyz = half3(u_xlat16_0) * u_xlat16_1.xyz;
    u_xlat2.x = dot(input.TEXCOORD0.xyz, float3(u_xlat16_1.xyz));
    u_xlat2.y = dot(input.TEXCOORD1.xyz, float3(u_xlat16_1.xyz));
    u_xlat2.z = dot(input.TEXCOORD2.xyz, float3(u_xlat16_1.xyz));
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat2.xyz;
    u_xlat2.xyz = (-input.TEXCOORD3.xyz) + FGlobals._WorldSpaceLightPos0.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat0.xxx * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat16_3.xyz = half3(u_xlat0.xxx * float3(u_xlat16_3.xyz));
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat5.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat0.xyz = fma(u_xlat1.xyz, (-float3(u_xlat15)), u_xlat0.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_1 = _MainTex.sample(sampler_MainTex, input.TEXCOORD4.xy);
    u_xlat0.y = (-float(u_xlat16_1.w)) + 1.0;
    u_xlat0.x = unity_NHxRoughness.sample(samplerunity_NHxRoughness, u_xlat0.xy).x;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat16_4.xyz = u_xlat16_1.xyz + half3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_5 = _MetallicTex.sample(sampler_MetallicTex, input.TEXCOORD4.xy).x;
    u_xlat16_4.xyz = fma(half3(u_xlat16_5), u_xlat16_4.xyz, half3(0.220916301, 0.220916301, 0.220916301));
    u_xlat16_18 = fma((-u_xlat16_5), half(0.779083729), half(0.779083729));
    u_xlat16_4.xyz = half3(u_xlat0.xxx * float3(u_xlat16_4.xyz));
    u_xlat16_4.xyz = fma(u_xlat16_1.xyz, half3(u_xlat16_18), u_xlat16_4.xyz);
    output.SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    output.SV_Target0.w = half(1.0);
    return output;
}
                                FGlobals�   	      _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        _LightPositionRange                          _LightProjectionParams                    0      _LightShadowData                 @      unity_ShadowFadeCenterAndType                     P      _LightColor0                 �      unity_MatrixV                    `      unity_WorldToLight                   �       	      _TerrainHolesTexture                 _MainTex                _MetallicTex                _TerrainNormalmapTexture                _LightTextureB0                 _LightTexture0                  unity_NHxRoughness                   _ShadowMapTexture                   FGlobals           